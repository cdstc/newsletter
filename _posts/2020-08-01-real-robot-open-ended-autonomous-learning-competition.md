---
title: "REAL 2020 – Robot open-Ended Autonomous Learning competition"
shorttitle: 

date: 2020-08-01
dateend: 
location: 
link: https://www.aicrowd.com/challenges/real-robots-2020
venue: IEEE Joint International Conference on Development and Learning and Epigenetic Robotics (ICDL-EpiRob 2020) 
organizers: Emilio Cartoni, Davide Montella, and Gianluca Baldassarre

journal: 
publisher: 

tf: [Action and Perception]
tags: [competition]
submissions: 
accepted: 
speakers: 
participants: 
---

