---
title: "Special Issue on: A Sense of Interaction in Humans and Robots: From Visual Perception to Social Cognition"
shorttitle: "SI IEEE TCDS: A Sense of Interaction in Humans and Robots"

journal: IEEE Transactions on Cognitive and Developmental Systems
publisher: 
date: 2018-12-07
location: 
link: https://ieeexplore.ieee.org/document/8567856
organizers: Alessandra Sciutti and Nicoletta Noceti

tf: [Action and Perception]
tags: [special issue]
submissions: 
accepted: 
speakers: 
participants: 
---

