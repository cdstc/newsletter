---
title: "SI IEEE TCDS: Continual Unsupervised Sensorimotor Learning"
shorttitle: 

journal: IEEE Transactions on Cognitive and Developmental Systems
date: 2019-09-30
location: 
link: https://projects.au.dk/socialrobotics/news-events/show/artikel/special-issue-on-continual-unsupervised-sensorimotor-learning/
organizers: Nicolás Navarro-Guerrero, Sao Mai Nguyen, Erhan Öztop and Junpei Zhong

tf: [Action and Perception]
tags: [Special Issue]
submissions: 13
accepted: 10
speakers: 
participants: 
---

