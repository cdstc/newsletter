---
title: "Special Issue: Control Design for Systems Operating in Complex Environments"
shorttitle: "SI Hindawi Complexity: Control Design for Systems Operating in Complex Environments"

journal: Complexity
publisher: Hindawi
date: 2019-02-12
location: 
link: https://www.hindawi.com/journals/complexity/si/462912/
organizers: Chenguang Yang, Zhaojie Ju, Xiaofeng Liu, Junpei Zhong, and Andy Annamalai

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

