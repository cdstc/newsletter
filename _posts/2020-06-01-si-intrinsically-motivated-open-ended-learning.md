---
title: "SI: Intrinsically Motivated Open-Ended Learning"
shorttitle: 

journal: IEEE Transactions on Cognitive and Developmental Systems 
date: 2020-06-01
location: 
link: https://ieeexplore.ieee.org/document/9113788
organizers: Kathryn Kasmarik, Gianluca Baldassarre, Vieri Giuliano Santucci, and Jochen Triesch

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

