---
title: "BRAIN-PIL Workshop on New advances in Brain-inspired Perception, Interaction and Learning"
shorttitle: 

journal: 
publisher: 
venue: IEEE International Conference on Robotics and Automation (ICRA) 
date: 2020-05-31
dateend: 
location: Virtual – Paris, France
link: https://brain-pil.github.io/icra2020/
organizers: Pablo Lanillos, Mohsen Kaboli, Natalia Díaz Rodríguez, David Filliat, and Tansu Celikel

tf: [Action and Perception]
tags: [workshop]
submissions: 17
accepted: 9
speakers: 
participants: 250 via ZOOM, 860 via youtube
---

