---
title: "IEEE SMC Special Session on Adaptation and Personalization in Human-Robot Interaction"
shorttitle: 

journal: 
venue: "IEEE International Conference on Systems, Man, and Cybernetics (IEEE SMC)"
date: 2019-08-08
dateend: 2019-08-11
location: Bari, Italy
link: http://www.smc2019.org/approved_special_sessions.html
organizers: B. De Carolis, C. Gena, A. Lieto, S. Rossi & A. Sciutti

tf: [Action and Perception]
tags: [Special Session]
submissions: 
accepted: 6
speakers: 
participants: 
---

