---
title: "ECML/PKDD International Workshop on Active Inference"
shorttitle: 

journal: template
publisher: template
venue: "European Conference on Machine Learning and Principles and Practice of Knowledge Discovery in Databases (ECML/PKDD)" 
date: 2020-09-14
dateend: 
location: Ghent, Belgium
link: https://iwaiworkshop.github.io/
organizers: Tim Verbelen, Cedric De Boom, Pablo Lanillos, and Christopher Buckley

tf: [Action and Perception]
tags: [Workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

