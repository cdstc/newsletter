---
title: "UMAP Workshop on Adapted intEraction with SociAl Robots (cAESAR)"
shorttitle: 

date: 2021-06-21
dateend: 
location: Virtual
link: https://caesar2021.di.unito.it/
venue: Conference on User Modeling, Adaptation and Personalization (UMAP) 
organizers: Berardina (Nadja) De Carolis, Cristina Gena, Antonio Lieto, Silvia Rossi, Alessandra Sciutti

journal: 
publisher: 

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 6
speakers: 1
participants: 
---

