---
title: "Workshop on Intrinsically Motivated Open-Ended Learning (IMOL)"
shorttitle: 

venue: 
date: 2022-04-01
dateend: 
location: TBA
link: 
organizers: Georg Martius, Vieri Giuliano Santucci, Rania Rayyes, Gianluca Baldassarre

tf: [Action and Perception]
tags: [Summer School, workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

