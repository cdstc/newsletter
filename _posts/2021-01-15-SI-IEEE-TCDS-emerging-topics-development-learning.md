---
title: "SI IEEE TCDS: Emerging Topics on Development and Learning"
shorttitle: 

journal: IEEE Transactions on Cognitive and Developmental Systems
date: 2021-01-15
location: 
link: https://cdstc.gitlab.io/icdl-2020/calls/special-issue/
organizers: María-José Escobar, Nicolás Navarro-Guerrero, Javier Ruiz-del-Solar and Giulio Sandini

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

