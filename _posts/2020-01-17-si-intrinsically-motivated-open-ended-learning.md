---
title: "SI: Intrinsically Motivated Open-Ended Learning in Autonomous Robots"
shorttitle: 

journal: Frontiers in Neurorobotics
date: 2020-01-17
location: 
link: https://www.frontiersin.org/articles/10.3389/fnbot.2019.00115/full
organizers:  Vieri Giuliano Santucci, Pierre-Yves Oudeyer, Andrew Barto and Gianluca Baldassarre

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

