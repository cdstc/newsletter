---
title: "RSS2021 Workshop on Declarative and Neurosymbolic Representations in Robot Learning and Control"
shorttitle: 

venue: 
date: 2021-07-12
dateend: 2021-07-16
location: Virtual Event
link: https://dnr-rob.github.io/
organizers: Alper Ahmetoglu, Shiqi Zhang, Roderic Grupen, Yuqian Jiang, Matteo Leonetti, Tiffany Liu, Erhan Oztop, Justus Piater, Benjamin Rosman, Tadahiro Taniguchi, and Emre Ugur

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

