---
title: "Talk at seminar on A.I. and IoT: New Horizon"
shorttitle:

journal: 
date: 2019-05-08
dateend: 2019-05-10
location: Abidjan, Cote d'Ivoire
link: http://univ-cotedazur.fr/en/eur/ds4h/contents/news/neurostic-2019
organizers: ESATIC

tf: [Action and Perception]
tags: [seminar]
submissions: 
accepted: 
speakers: 
participants: 200
---

Sao Mai Nguyen gave a plenary talk at the Seminar on A.I. and IoT: New Horizon, organised by ESATIC under the patronage of the minister of Digital Economy and the Post of Ivory Coast, Claude Isaac De.
