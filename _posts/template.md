---
title: "template"
shorttitle: 

date: template
dateend: 
location: template
link: template
venue: template 
organizers: template

journal: template
publisher: template

tf: [Action and Perception, Developmental Psychology, Human-Robot Interaction, Language and Cognition, Robotics, Evolutionary-Developmental Systems and Robotics]
tags: [conference, workshop, special issue, book, talk]
submissions: 
accepted: 
speakers: 
participants: 
---

